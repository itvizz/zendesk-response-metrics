$("#card").hide();

var client = ZAFClient.init();

client.invoke('resize', { width: '100%', height: '350px' });

getTicketID();

function getTicketID() {
    client.get('ticket.id').then(function (data) {
        var ticketId = data['ticket.id'];
        requestUserInfo(client, ticketId);
    });
}

//Call the metrics API for the current ticket
function requestUserInfo(client, id) {
    var settings = {
        url: '/api/v2/tickets/' + id + '/metrics.json',
        type: 'GET',
        dataType: 'json',
    };

    client.request(settings).then(
        function (data) {
            
            //console.log(data);
            //console.log(data['ticket_metric']['assigned_at']);

            $("#lblreplytimebusiness").text(data['ticket_metric']['reply_time_in_minutes']['business']);
            $("#lblreplytimecalendar").text(data['ticket_metric']['reply_time_in_minutes']['calendar']);
            $("#ptotalreplies").text(data['ticket_metric']["replies"]);
            $("#ptotalreopens").text(data['ticket_metric']["reopens"]);
            $("#loader").hide();
            $("#card").show();
                
        },
        function (response) {
            console.error(response);
        }
    );

}  